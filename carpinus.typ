#show link: underline

= Carpinus: Understanding Central Limit Theorems for Dependent Data

== Motivation

Causal inference with interference relaxes
the strict assumption of no-interference between units.
In this setting,
exposures of other units may affect the outcome of a focal unit.
This setting clearly implies some dependency 
among the random variables of study units.
Many statistical estimators typically
assume (statistical) independence between units.
Properties of the variance of these estimators
often (though not always) 
depend on central limit theorems for independent data.
Central limit theorems for dependent data exist, however, 
and we wish to understand them in order to
derive variance estimators
that do not depend on an independence assumption,
as done in 
@Chin2018centralLi,
@Ogburn2017causalInf,
@Li2022,
and @Wang2020.

== Goals

Stein's method for distributional approximation 
has proven useful for this problem.
We'll start there.

- An expository paper targeted to causal inference 
  practicioners similar to @Li2017generalFo.
- (?) formal proofs of Stein "stuff" à la @Avigad2017.
- (?) toward computational tools for automatically _and correctly_
  applying Stein's method.

== Background

=== What is a "central limit theorem"?

- "These days the term 'central limit theorem'
   is associated with a multitude of statements
   having to do with the convergence of probability distributions of functions
   of an increasing number of one- or multi-dimensional random variables
   or even more general random elements 
   (with values in Banach spaces or more general spaces) 
   to a normal distribution (or related distributions). @Fischer2011

Central limit theorems are about
the distribution 
of a (normed) series
of random variables,
where the limit 
is as the length sequence of random variables goes to $infinity$.
I presume that in most references herein
that by "random variable", 
authors mean a random variable whose codomain is $RR$.
I'll make the same assumption,
except where explicit.

=== Stein's method: What/Why? #footnote[
  See videos from
  #link("https://steinworkshop.github.io/")[
    Workshop on Stein's Method in Machine Learning and Statistics]
  for more materials.
]

"Stein’s method was initially conceived
by Charles Stein in the seminal paper
to provide errors in the approximation by the normal distribution
of the distribution
of the sum of dependent random variables
of a certain structure.
However,
the ideas presented in [Stein's paper] are
sufficiently abstract and powerful
to be able to work well beyond that intended purpose,
applying to approximation of more general random variables
by distributions other than the normal
(such as the Poisson, exponential, etc)."
@Ross2011fundamenta

Two components of Stein's method @Ross2011fundamenta:

- "a framework to convert the problem of bounding the error
  in the approximation of one distribution of interest
  by another, well understood distribution 
  (e.g. the normal) 
  into a problem of bounding the expectation
  of a certain functional of the random variable of interest."
- "collection of techniques
  to bound the expectation appearing in the first component; 
  Stein appropriately refers to this step as 'auxiliary randomization.'"

=== Basic intuition

Stein explains the problem well in @Stein1986approximat:

"The problem of computing expectations,
perhaps only approximately,
can be divided into two parts.
Let $(Omega, BB, P)$ be a probability space
and $E : Chi |-> RR$
the expectation mapping associated with $P$
on the linear space $Chi$
of all real random variables $Z : Omega |-> RR$
having finite expectations.
In order to approximate $E Z$
for such a $Z$
we may first determine

$
ker E = {Y : E Y = 0}
$

and then search $ker E$ for a random variable
that is approximately $Z - c$
for some constant $c$.
We can conclude that 

$
E Z approx c
$"

In brief, the two problems are:

+ find the kernel 
   #footnote[ 
      "The kernel can be viewed as a measure of the degree
      to which the homomorphism fails to be injective."
      See #link("https://en.m.wikipedia.org/wiki/Kernel_(algebra)")[
      Kernel (algebra) on wikipedia
  ]] 
  of the $E$ homomorphism.
+ search the kernel for another random variable
  that is within some distance of $Z$. 

Conjectures:

+ M-estimation is a special case of Stein's method.
  Note that root finding is searching the kernel.
+ $RR$ is more structure than is needed.
  What is the meaning of stein's method for arbitrary semirings?
+ This approach is somehow related to derivatives,
  since derivatives and expectation are so tightly related
  (see section 5 of @Li2009firstAnd).
  Does this perhaps suggest a connection to influence functions?
  The auxiliary randomization method
  that Stein laid out also hints at a connection to influence functions.

Here's the basic intuition behind Stein's method for normal approximation
as explained in @Chen2010steinCoup.
First, we know that for all 
#link("https://en.wikipedia.org/wiki/Lipschitz_continuity")[
  Lipschitz continuous functions
]
(e.g. $L_1 := {f : | f(x) - f(y) | <= | x - y | }$)
,

$
E[Z f(Z) ] = E [f'(Z)] <==> Z tilde.op N(0 , 1)
$

thus, if for some random variable $W$,

$
E[W f(W) ] approx E [f'(W)]
$

for many $f$
then we would expect that the distribution $W$ is
close to the distribution of $Z$.

=== A bit more rigorous (for Normal case)

Let's measure the discrepency between the laws of $W$ and $Z$ by
$E h(W) - E h(Z)$ for some function $h$.

If $W tilde.op N(0, 1)$,
then $ E [f'(W)] - E[W f(W) ] = 0 = E h(W) - E h(Z)$.
Define the (higher-order) function $A$
as $A(f)(x) = f'(x) - x f(x)$.
Thus, $E {A(f(W))}$ measures the discrepency between $W$ and $Z$.

==== Stein identities

Suppose $exists$ random variables $T_1$ and $T_2$ such that

#math.equation(
  block: true,
  numbering: "(1.1)",
  supplement: none,
  $E[W f(W) ] = E [T_1 f'(W + T_2)]$
) <stein-identity>

@stein-identity is a characterization of a the distribution of $W$,
and the game is about finding $T_1$ and $T_2$.

For example,
if $T_2 = 0$, 

$
E {A(f(W))} 
   &= E { f'(W) - W f(W) } \
   &= E { f'(W) - T_1 f'(W) } \
   &= E { (1 - T_1) f'(W) } \
$

==== Stein coupling 

@Chen2010steinCoup propose a general framework:
"We introduce the new concept of _Stein couplings_
and we show that it lies at the heart of popular approaches
such as the local approach, exchangeable pairs, size biasing
and many other approaches."

Let $(W, W', G)$ be a coupling 
#footnote[
  #link("https://mathematicaster.org/teaching/lcs22/hollander_coupling.pdf")[
    WTF is a coupling?
  ]
]
(tripling (?))
of square integrable random variables.
This triple is called a Stein coupling if

$
E{ G f (W') - G f (W) } = E { W f (W)}
$

for all functions for which expectations exist.


=== Examples

Example usage of Stein's method from Theorem 1 of @Ogburn2017causalInf:

"For the proof that the first order terms converge to a normal distribution,
we rely on Stein’s method of central limit theorem proof (Stein, 1972). 
Stein’s method allows us to derive a bound
on the distance between our first order term (properly scaled)
and a standard normal distribution;
this bound depends on the degree distribution $K_1 , ..., K_n$. 
We show that this bound converges to 0 
as $n → ∞$ under regularity conditions
and our running assumption that $K^2_max,n = o(n)$.

In short:

- use Stein's method to derive bounds
  on distance between distribution of target random variable
  and some useful distribution (i.e. standard normal)
- then show the bounds converge to 0 as $n arrow infinity$.

In length, see section 10 of @Ogburn2017causalInf.
Example result:

- "Lemma 1 (Applying Stein’s Method to the dependent sum). 
  Consider a network of nodes given by adjacency matrix $A$.
  Let $U_1 , ..., U_n$ 
  be bounded mean-zero random variables 
  with finite fourth moments and with dependency neighborhoods
  $D_i = i 
     union {j : A#sub[ij] = 1} 
     union {k : A#sub[jk] = 1 "for" j : A#sub[ij] = 1}$,
  and let $K_i$ be the degree of node $i$. 
  Note that the size of each dependency neighborhood is bounded above
  by $K^2#sub[max,n]$. 
  If $K_i = K#sub[max,n]$ for all $i$ and 
  $K^2#sub[max,n] / n → 0$,
  then $sqrt (frac(sum U_i, sqrt("var" sum U_i))) arrow N(0 ,1 )$."

= References

#hide[
  #cite(
    "Anastasiou2021steinSMe"
  , "Stein1986approximat"
  , "Ley2014approximat"
  , "Fang2015ratesOfC"
  , "Chen2010steinCoup"
  , "Bardet2008dependent"
  , "Chang2021"
  , "Davies2014"
  , "Dedecker2002maximalIn"
  , "Durieu2011"
  , "Fleermann2022"
  , "Hoeffding1948"
  , "Hill2009"
  , "Li2022"
  , "Park2009"
  , "Romano2000"
  , "Shang2012"
  , "Wang2020"
  , "Barron1986"
  , "Hoeffding1951"
  , "Pollard1985newWaysT"
  , "Fang2021highDimen"
  , "Rio2017"
  , "Kojevnikov2019limitTheo"
  , "Shang2012aNoteOn"
  , "Wang2014sumOfArb"
  , "Chandrasekhar2023generalCo"
  , "Leung2022causalInf"
  ) 
]

#bibliography("carpinus.bib", title: none)

== Misc Notes

- finite population vs infinite population #footnote[
I have a hunch that finite vs infinite populations are dual concepts
connected to the categorical/type theoretic concepts of
(data (canonically lists) vs codata (canonically streams)).
#link("https://en.wikipedia.org/wiki/Coinduction")[
  See wikipedia entry for Coinduction.]
]
  - "The finite population asymptotic scheme embeds $Pi_N$
    into a hypothetical infinite sequence
    of finite populations with increasing sizes,
    and the asymptotic distribution of any sample quantity is
    its limiting distribution
    along this hypothetical infinite sequence" @Li2017generalFo

Compositional nature of central limit theorem is more clear
using "function" notation.

Let $X_i : S arrow.r.long.bar RR$
be independent and identically distributed random variables
with distribution $mu$ and variance $sigma^2$.
The result of the iid CLT can be expressed as (@rvstack):

$
op("lim", limits: #true)_(n -> infinity)
[
f (sqrt n / sigma) 
compose g xi 
compose f (1 / n) 
(mu ^ (*n) (( - infinity , x]))
]
= Phi (x).
$
