{
  description = "Carpinus";
  nixConfig = {
    bash-prompt = "λ ";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
    typst = {
      url = "github:typst/typst";
      # Use the same nixpkgs
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, typst}:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};

      projectName = "carpinus";

      typst' = typst.packages.${system}.default;
    in {

      packages.paper = pkgs.stdenv.mkDerivation {
        name = "${projectName}";
        version = "";
        src = ./.;
        buildInputs = [ typst' ];
        buildPhase = ''
          typst c --root . ${projectName}.typ
        '';
        installPhase = ''
          mkdir -p $out
          cp ${projectName}.pdf $out/
        '';
      };

      defaultPackage = self.packages.${system}.paper;

      devShells.default =  pkgs.mkShell {
        buildInputs = [

          # Documentation/writing tools
          pkgs.typst-lsp
          pkgs.typst-fmt
          typst'
          # pkgs.pandoc
        ];
      }; 
    });
}
