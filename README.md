---
title: "Understanding Central Limit Theorems for Dependent Data"
languages: [nix]
---


## Acknowledgements

GitLab avatar of _Carpinus caroliniana_ is from
[Wikimedia commons](https://commons.wikimedia.org/wiki/File:Carpinus_caroliniana_Grab_ameryka%C5%84ski_2019-06-01_03.jpg).
